<?php 
class Data {

	public $source = "https://www.readability.com/rseero/latest/feed";
	public $url_tag = "https://www.readability.com/api/content/v1/parser?url=";
	public $def_image = "http://ctmortgageloans.com/wp-content/uploads/2015/02/no-image-300x300.png";
	
	function getImage($url){
		if(isSet($_GET["show_images"])){
			$request_url = explode("url=", $url)[1];
			$api_key = "75f68dc0b1924bc42ad2de85b68ed8dd6038499a";
			$request_url = $this->url_tag.$request_url."&token=$api_key";
			$xmlString = file_get_contents($request_url);
			$xml = json_decode($xmlString);
			$external_link = $xml->lead_image_url;
			if (@getimagesize($external_link)) {
				return $xml->lead_image_url;
			} else {
				return $this->def_image;
			}
		}
        return $this->def_image;
    } 
	
	function getUrl(){
		return $this->source;
	}
	
	function getXML(){
		try{
			$xmlString = file_get_contents($this->source);
			$xml = new SimpleXMLElement($xmlString);
			foreach($xml->channel->item as $item){
				$xml->channel->item[$counter]->image = $this->getImage($item->link);
			}
		}catch (Exception $e) {
			return array();
		}
        return $xml->channel;
    } 
}
$data = new Data();
//var_dump($data->getXML());
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>Static Top Navbar Example for Bootstrap</title>
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
	<style type="text/css">
		.item{
			height:300px;
			overflow:hidden;
			cursor:pointer;
			padding:5px;
		}
		.media-object{
			width: 128px; height: 128px;
			max-height: 108px;
			max-width: 108px;
			display:inline-block;
			float:right;
		}
		.media-object img, .main_img{	
			background-size:contain;
			max-height: 100%;
			max-width: 100%;
		}
		#news{overflow:auto;}
		.loader {
			background-color:rgba(255, 255, 255, 1);
			width:100%;
			height:100%;
			position: absolute;
			z-index: 9999;
		}
		.media-heading{
			word-wrap: break-word;
		}
		.spinner {
			border: 5px solid #f3f3f3; /* Light grey */
			border-top: 5px solid #3498db; /* Blue */
			border-radius: 50%;
			width: 120px;
			height: 120px;
			margin:5% auto;
			animation: spin 0.5s linear infinite;
		}

		@keyframes spin {
			0% { transform: rotate(0deg); }
			100% { transform: rotate(360deg); }
		}
		@media screen and (max-width: 768px){
			.media-object{
				float:left !important;
				padding:5px 10px;
			}
			.media-heading{
				padding:5px;
			}
			.desc{display:none;}
			.item{
				float:none;
				clear:both;
				height:auto;
				border-bottom: 1px solid #EEEEEE;
			}
		}
		.navbar-default .navbar-nav > li > a{
			color:#fff;
		}
		.navbar-default{
			color: #fff;
			background-color: #39b3d7;
			border: none;
		}
		.navbar-default .navbar-nav > .dropdown > a .caret{
			border-top-color: #fff;
			border-bottom-color: #fff;
		}
		.navbar-default .navbar-brand{
			color:#fff;
		}
	</style>
  </head>
  <body>
    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">U24</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Uudised</a></li>
            <li><a href="#">Telli digileht</a></li>
            <li><a href="#about">Telli paberleht</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Logi sisse</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
       <?php
		if(count($data->getXML())==0){echo("<b>NB!</b> Lehelt <a href=".$data->getUrl().">".$data->getUrl()."</a> ei §nnestunud infot hankida");}
			
		foreach($data->getXML()->item as $item){?>
			<div class="col-sm-3 item" data-link="<?=$item->link?>" data-toggle="modal" data-target="#newsModal">
				<div class="media-object"><img alt="128x128" data-src="holder.js/128x128" src="<?=$item->image?>" data-holder-rendered="true" ></div>
				<h4 class="media-heading"><?=$item->title?></h4> 
				<p class="desc"><?=$item->description?></p>
			</div>
	   <?php } ?>

    </div> <!-- /container -->

	<div class="modal fade" id="newsModal" tabindex="-1" role="dialog">
	  <div class="modal-dialog modal-lg" role="document">
		<div class="loader"><div class="spinner"></div></div>
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title">Pealkiri</h4>
		  </div>
		  <div class="modal-body" id="news">
			<h1>Laadimine</h1>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Sulge</button>
		  </div>
		</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
	<script type="text/javascript">
	
		$(".item").click(function(){
			
			$("#news").html("<h1>Laadimine</h1>");
			var url = $(this).attr("data-link");
			$(".loader").show();
			
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url: "request.php", //Relative or absolute path to response.php file
				data: {url: url},
				success: function(response) {
					$(".modal-title").text(response.title);
					var new_html = "<div class='row'><div class='col-sm-12'> " + response.content +"</div></div>";
					$("#news").html(new_html);
					$(".loader").hide();
					/*$.each( response, function( key, value ) {
						this_html += ( "<div class='col-sm-3'>" + key + "</div><div class='col-sm-9'> " + response[key] +"</div>");
					});*/
				}, error:function(response) {
					console.log(response);
				}
			});
		
		});
		
	</script>
  </body>
</html>