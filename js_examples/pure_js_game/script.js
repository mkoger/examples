	
var step = 50, block_width = 3, timer = speed = 1, score = side_block_counter = 0;
var car_pos = 5 * step, car_top = 10*step, car_bottom = 14*step;
var side_blocks = [];
var road_blocks = [];

car = document.getElementById('car');
car.style.left = car_pos + "px";
car.style.top = car_top + "px";
car.style.width = 2 * step + "px";

var score_dom = document.getElementById('score');
var speed_dom = document.getElementById('speed');
document.onkeydown = keyEvent; //klahvivajutuse kuulaja
	
	
function gameOver(){
	alert("M�ng l�bi, sinu skoor oli: "+score);
	location.reload();
}

// funktsioon, kus kontrollitakse, et auto ei oleks teeblokiga kokku p�rganud
function checkCollusion(block){
	car.left = parseInt(car.style.left);
	car.right = car.left + parseInt(car.style.width);
	block.top = parseInt(block.dom.style.top);
	block.bottom = block.top + step;
	if(
		((block.left < car.left && block.right > car.left) ||
		(block.right > car.right && block.left < car.right)) &&
		block.bottom > car_top && block.top < car_bottom
	){		
		gameOver();
	}
}

// Bloki loomise funktsioon
function sideBlock(left, counter){
	this.top = counter * step * 4;
	this.dom = document.createElement("div"); 
	this.dom.setAttribute("class", "side_block");
	this.dom.style.left = left + "px";
	this.dom.style.top = this.top + "px";
	document.getElementById('field').appendChild(this.dom);
}

// Bloki loomise funktsioon
function roadBlock(){
	var position = Math.floor((Math.random() * 9) + 1);
	this.top = 0;
	this.left = position * step;
	this.right = this.left + block_width * step;
	this.dom = document.createElement("div"); 
	this.dom.setAttribute("class", "block");
	this.dom.style.left = this.left + "px";
	this.dom.style.top = this.top;
	document.getElementById('field').appendChild(this.dom);
}

// Vasaku v�i parema klahvi vajutusel liigutatakse autot.
function turn(evt) {
	switch (evt.keyCode) {
		case 37:
			car.style.left = parseInt(car.style.left) - step + 'px';
		break;
		case 39:
			car.style.left = parseInt(car.style.left) + step + 'px';
		break;
	}
}

// Klahvi kuulaja funktsioon
function keyEvent(e) {
	e = e || window.event;
	turn(e);
	// kui keerati m�ngualast v�lja on m�ng l�bi
	if(parseInt(car.style.left) < 0 || parseInt(car.style.left) > 500){
		gameOver();
	}
	for(i in road_blocks){
		if(road_blocks[i]){
			checkCollusion(road_blocks[i]);
		}
	}
}

// Blokkide liigutamine ja tekitamine s�ltuvalt kiirusest
function game(){
	
	// Timer uue teebloki tekitamise tarbeks vahemikus (5-8)
	if(timer == 9){
		timer = Math.floor((Math.random() * 4) + 1); 
		var road_block = new roadBlock();
		road_blocks.push(road_block);
	} else{	timer ++;}	
	
	// ��rekivide asukoht nullitakse kui on tee l�ppu j�udnud
	for(s in side_blocks){
		if(parseInt(side_blocks[s].dom.style.top) > 700){
			side_blocks[s].dom.style.top = "0px";
		} else{
			new_pos = parseInt(side_blocks[s].dom.style.top) + step;
			side_blocks[s].dom.style.top = new_pos + 'px';
		}
	}
	
	// Eemaldatakse kiiruse eesm�rgil need teeblokid, mis on pildilt kadunud ja suurendatakse skoori.
	for(i in road_blocks){
		if(parseInt(road_blocks[i].dom.style.top) > 600){
			road_blocks[i].dom.remove();
			road_blocks.splice(i, 1);
			score ++;
			score_dom.innerHTML=score;
		}
		new_pos = parseInt(road_blocks[i].dom.style.top) + step;
		road_blocks[i].top = new_pos;
		road_blocks[i].dom.style.top = new_pos + 'px';
		checkCollusion(road_blocks[i]);
	}
}

// Iga 20 sekundi j�rel suurendatakse m�ngu kiirust
setInterval(function(){
	speed ++;
	speed_dom.innerHTML=speed;
	clearInterval(game);
	timer = setInterval(game, 500/(speed/4));
}, 20000);

//Luuakse ��rekivid
while(side_block_counter < 4){
	var side_block = new sideBlock(-50, side_block_counter);
	side_blocks.push(side_block);
	var side_block = new sideBlock(600, side_block_counter);
	side_blocks.push(side_block);
	side_block_counter ++;
}

setInterval(game, 500);
var block = new roadBlock();
road_blocks.push(block);
