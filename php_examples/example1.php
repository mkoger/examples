<?php
require("functions.php");

//$_SESSION["lang"] = isSet($_SESSION["lang"]) ? $_SESSION["lang"] : "et";
$interval = "15";
$timezone = strlen($_POST["timezone"]>2) ? $_POST["timezone"] : "Europe/Tallinn";
date_default_timezone_set($timezone);
$mindatetime = new DateTime();
$date = $_POST["date"];
$newfrom = strlen($_POST["from"]>2) ? $_POST["from"] : "05:00";
$newto = strlen($_POST["to"]>2) ? $_POST["to"] : "23:00";
$timestart = new DateTime($date." ".$newfrom.":00");
$timeend = new DateTime($date." ".$newto.":00");
$serviceID = $_POST["service"];
$location = $_POST["location"];
$language = $_COOKIE["lang"];
$pricefrom = $_POST["pricefrom"];
$priceto = $_POST["priceto"];
$workingday = strtolower(date('D', strtotime($date)));
$employees = array();
$code = "";
$employs = array();
$resultArray = array();
$serviceArray = array();
$ratingsArray = array();
$employee_services = array();
$serviceData = getObject("SELECT * FROM service WHERE svc_id=".$_POST["service"]);
$services = explode(",", $serviceData->et_abbr);
$notlike = explode(",", $serviceData->et_notlike);
$andlike = explode(",", $serviceData->et_andlike);
foreach($services as $k => $v){$services[$k] = " et LIKE '%$v%' ";}
foreach($notlike as $k => $v){$notlike[$k] = " et NOT LIKE '%$v%' ";}
foreach($andlike as $k => $v){$andlike[$k] = " et LIKE '%$v%' ";}
$sql_service = implode("OR", $services);
$sql_notlike = implode("AND", $notlike);
$sql_notlike = count($notlike)>1 ? " AND ".$sql_notlike : "";
$sql_andlike = implode("AND", $andlike);
$sql_andlike = count($andlike)>0 ? " AND ".$sql_andlike : "";

$sql_svcs = "SELECT svc_id FROM service WHERE ".$_POST["lang"]." != '' AND ($sql_service) $sql_notlike $sql_andlike";
$find_services = arrayString($sql_svcs, "svc_id", ", ");
$services = $yhendus->query("Select * from calendar_service WHERE service_id IN ($find_services)");

while($service = $services->fetch_assoc()) {
	$svc_info = getObject("SELECT * FROM service_details WHERE service_id=".$service["service_id"]. 
	((int)$pricefrom > 1 ? " AND price >= $pricefrom" : "").  
	((int)$priceto > 1 ? " AND price <= $priceto" : ""));
	if($svc_info != false){	
		array_push($employs, $service["calendar_id"]);
		$service_name = getObject("SELECT * FROM service WHERE svc_id=".$service["service_id"])->$language;
		
		if( isSet($serviceArray[$service["calendar_id"]]) && strlen($serviceArray[$service["calendar_id"]]["service"]) < strlen($service_name)   ){
			
		} else{
			$serviceArray[$service["calendar_id"]] = array(
				"service" => $service_name,
				"price" => $svc_info->price,
				"duration" => strlen($service["duration"])>3 ? $service["duration"] : $svc_info->duration
			);
		}
		
		
	}
}

//Kõik töötajad, kes sobivad ja on tööl
$calendars = getArray("Select calendar.user_email, user, phone, intervaly, calendar.calendar_id, cal_name, city, state, address,
	".$workingday."_from, ".$workingday."_to, company, link, website
	FROM calendar, openTimes, userSettings
	WHERE calendar.user_email = userSettings.user AND calendar.calendar_id = openTimes.cal_id
	AND keskkond = 1
	AND DATE(expiration) > '".date("Y-m-d")."'
	AND (state LIKE '%".$location."%' || city LIKE '%".$location."%')
	AND borough LIKE '%".$_POST["borough"]."%'
	AND calendar_id IN(".implode(', ', $employs).")");

$company_list = getArray("Select calendar.user_email
	FROM calendar, openTimes, userSettings
	WHERE calendar.user_email = userSettings.user AND calendar.calendar_id = openTimes.cal_id
	AND keskkond = 1
	AND DATE(expiration) > '".date("Y-m-d")."'
	AND (state LIKE '%".$location."%' || city LIKE '%".$location."%')
	AND borough LIKE '%".$_POST["borough"]."%'
	AND calendar_id IN(".implode(', ', $employs).") GROUP by calendar.user_email");

$locations = array();
foreach($calendars as $calendar){
	$locations[$calendar["user"]] = $calendar["address"];
	$timeFrom = "00:00";
	$timeTo = "00:00";
	$scheduleTime = getObject("Select timefrom, timeto FROM schedule WHERE calendar_id=".$calendar["calendar_id"]." AND date='".$date."'");
	if ($scheduleTime != false) {
		$timeFrom = $scheduleTime->timefrom;
		$timeTo = $scheduleTime->timeto;
	}else{
		$timeFrom = $calendar[$workingday."_from"];
		$timeTo =  $calendar[$workingday."_to"];
	}
	if($timeFrom != "00:00"){
		$timestart = new DateTime($date." ".$newfrom.":00");
		$objectDetails = explode(":", $serviceArray[$calendar["calendar_id"]]["duration"]);
		$calendar_start = new DateTime($date." ".$timeFrom.":00");
		$calendar_end = new DateTime($date." ".$timeTo.":00");
		//$calendar_end->sub(new DateInterval('PT'.$objectDetails[0].'H'.$objectDetails[1].'M'));
		$serviceEnd = new Datetime($timestart->format("Y-m-d H:i:s"));
		$serviceEnd->add(new DateInterval('PT'.$objectDetails[0].'H'.$objectDetails[1].'M'));
		
		$count = 0;	$points = 0;
		$ratings = getArray("Select * from ratings WHERE calendar_id=".$calendar["calendar_id"]);
		foreach($ratings as $rating){
			$count++; $points += $rating["rating"];
		}
		
		$user_bookings = $yhendus->query("Select booking_id from bookings WHERE calendar_id = ".$calendar["calendar_id"]." AND 
			email = '".$_COOKIE["user_email"]."' AND booking_date < '".date("Y-m-d H:i:s")."' AND verified!=10");
		
		$ratingsArray[$calendar["calendar_id"]] = array(
			"rows" => $ratings,
			"commentable" => ($user_bookings->num_rows > 0 && $_COOKIE["user_email"] != "" ? "true" : "false")
		);
		while($timestart < $timeend){
			
			if($calendar_start < $timestart && $calendar_end >= $serviceEnd && $timestart > $mindatetime){
				
				$from = $timestart->format("Y-m-d H:i:s");
				$to = $serviceEnd->format("Y-m-d H:i:s");
				
				$bookcount = $yhendus->query("Select booking_id from bookings WHERE verified!=10 AND calendar_id = ".$calendar["calendar_id"]." AND 
					(('$from' >= booking_date AND '$from' < dateend) OR 
					('$to' > booking_date AND '$to' <= dateend) OR 
					('$from' <= booking_date AND '$to' >= dateend))
				");
				
				if($bookcount->num_rows == 0 && $serviceArray[$calendar["calendar_id"]]["service"]!= ""){
					
					$hinnang = $count != 0 ? array($count, $points) : array(0,0);
			
					$resultArray[] = array(
						"time" => $timestart->format("H:i"),
						"timeValue" => $timestart->format("H").$timestart->format("i"),
						"price" => $serviceArray[$calendar["calendar_id"]]["price"],
						"duration" => $serviceArray[$calendar["calendar_id"]]["duration"],
						"date" => $date,
						"service" => $serviceID,
						"phone" => $calendar["phone"],
						"person" => $calendar["cal_name"],
						"calendar_id" => $calendar["calendar_id"],
						"svc_name" => $serviceArray[$calendar["calendar_id"]]["service"],
						"svc_length" => strlen($serviceArray[$calendar["calendar_id"]]["service"]),
						"company" => (strlen($calendar["website"]) > 2 ? "<a target='_blank' href='http://".$calendar["website"]."'>".$calendar["company"]."</a>" : $calendar["company"]),
						"link" => $calendar["link"],
						"rating" => $hinnang[1]/($hinnang[0] != 0 ? $hinnang[0] : 1),
						"ratingCount" => $hinnang[0],
						"ratings" => $ratings,
						"user" => $calendar["user"],
						"address" => $calendar["address"],
						"city" => $calendar["city"]
					);
					
				}
				
			}
				
			$timestart->add(new DateInterval('PT'.$calendar["intervaly"].'M'));
			$serviceEnd->add(new DateInterval('PT'.$calendar["intervaly"].'M'));
		}
	}
}

echo json_encode(array("code" => count($resultArray), "locations" => $locations, "array" => $resultArray, "companies" => json_encode($company_list)));

?>