<?php

class Data {
  
	public $url;
	public $cacheFile;
	public $data = array();
	public $form = array();
	public $error = "";

	public function __construct ( $form ) {
		$this->url = "http://domain.com/elect/?start=".$form["start"]."&end=".$form["end"];
		$this->form = $form;
		$this->cacheFile = "caches/".md5($this->url);
	}

	
	//Kontrollitakse vormis olevaid andmeid.
	public function validation(){
		if(!DateTime::createFromFormat('d-m-Y', $this->form["start"])){
			$this->error .= "Algusaja formaat on vale. ";
		}
		if(!DateTime::createFromFormat('d-m-Y', $this->form["end"])){
			$this->error .= "Lõppaja formaat on vale. ";
		}
		if(strtotime($this->form["start"]) < strtotime("-2 years")){
			$this->error .= "Algusaeg saab olla maksimaalselt 2 aasta vana. ";
		}
		if(strtotime($this->form["end"]) > strtotime("+1 day")){
			$this->error .= "Lõppaeg ei saa olla suurem tänasest. ";
		}
		if(strtotime($this->form["start"]) > strtotime($this->form["end"])){
			$this->error .= "Algusaeg peab olema suurem lõppajast. ";
		}
		if((float)$this->form["price"] == 0){
			$this->error .= "Hind peab olema suurem nullist. ";
		}
		return $this->error;
	}
	
	
	// Kui sama ajavahemiku päring on varem tehtud, tagastatakse cache.
	// Muul juhul tehakse päring ja andmed cache'takse.
	public function getCache(){
		if ( file_exists($this->cacheFile)) {
			$content = file_get_contents($this->cacheFile);
			$this->data = json_decode($content);
		} else {
			$array = simplexml_load_file($this->url);
			$consumtions = $array->AccountTimeSeries->ConsumptionHistory->HourConsumption;
			foreach($consumtions as $object){
				$this->data[] = array("".$object."", "".$object->attributes()->ts."");
			};
			file_put_contents($this->cacheFile, json_encode($this->data));
		}
		return $this->data;
	}
	
	
	// Kõik andmed, mis jäävad määratud ajavahemikku lisatakse grupeeritult massiivi. 
	public function processData(){
		$result = array();
		foreach($this->data as $object){
			$datetime = strtotime($object[1]);
			if ($datetime >= strtotime($this->form["start"]." 00:00:00") && $datetime < strtotime($this->form["end"]." 23:59:00")){
				$key = date($this->form["method"], $datetime);
				array_key_exists($key, $result) ? "" : $result[$key] = array(0, 0);
				$result[$key][0] += (float)$object[0];
				$result[$key][1] += (float)$object[0]*(float)$this->form["price"];
			}
		}
		return $result;
	}
}