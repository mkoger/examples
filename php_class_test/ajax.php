<?php
require("class.php");

$data = new Data($_POST);
$error = $data->validation();
$result = array();

//Kui sisendid on vormis korrektsed, siis tagastatakse andmed.
if($error == ""){
	$cache = $data->getCache();
	$result = $data->processData();
}

echo json_encode(array("data" => $result, "error" => $error));