<!--

JAVASCRIPTI OSA �hes suuremast projektist

N�iteks arvutatakse v�lja distantsid inimese praegusest asukohast v�i sisestatud aadressist 
GOOGLE MAPS API'iga ja palju muud.



-->


<script type="text/javascript" src="js/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="js/intlTelInput.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyAplIlY1r7AQJsvibD_ssVGud84mNiWDSQ"></script>
<script type="text/javascript">
	
function autoPlayYouTubeModal(){
	  var trigger = $("body").find('[data-toggle="modal"]');
	  trigger.click(function() {
		var theModal = $(this).data( "target" ),
		videoSRC = $(this).attr( "data-theVideo" ), 
		videoSRCauto = videoSRC+"?autoplay=1" ;
		$(theModal+' iframe').attr('src', videoSRCauto);
		$(theModal+' button.close').click(function () {
			$(theModal+' iframe').attr('src', videoSRC);
		});   
	  });
	}

var city = "<?=$city?>";
var geocoder = new google.maps.Geocoder();
var alldata = [];

<?php if(isSet($_GET["g"])) {?>
if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
} else{
	$("#orig").val("<?=$city?>, Estonia");
}
$('html, body').animate({
		scrollTop: $("#searchroom").offset().top
}, 500);
<?php } ?>
//Get the latitude and the longitude;
function successFunction(position) {
	var lat = position.coords.latitude;
	var lng = position.coords.longitude;
	codeLatLng(lat, lng)
}

function errorFunction(){
	//alert("Geocoder ei suutnud aadressi m��rata");
	$("#orig").val("<?=$city?>, Estonia");
}

var placeSearch, autocomplete;
var componentForm = {
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() {
	geocoder = new google.maps.Geocoder();
  autocomplete = new google.maps.places.Autocomplete(
	  (document.getElementById('autocomplete_addr')),
	  {types: ['geocode']});
  searchcomplete = new google.maps.places.Autocomplete(
	  (document.getElementById('orig')),
	  {types: ['geocode']});
	
  autocomplete.addListener('place_changed', fillInAddress);
  
  //updateDistance("Tallinn, Estonia");
}



function callback(response, status) {
	console.log(response);
	var orig = document.getElementById("orig"),
		dest = document.getElementById("dest"),
		dist = document.getElementById("dist");

	if(status=="OK") {
		count = 0;
		for(destin in alldata.locations){
			alldata.locations[destin] = response.rows[0].elements[count].status != "NOT_FOUND" ? response.rows[0].elements[count].distance.value/1000 : 9999;
			count ++;
		}
		console.log(alldata.locations);
		dest.value = response.destinationAddresses[0];
		//orig.value = response.originAddresses[0];
		dist.value = response.rows[0].elements[0].distance.text;
	} else {
		alert("Error: " + status);
	}
	for(j in alldata.array){
		alldata.array[j]["distance"] = alldata.locations[alldata.array[j]["user"]];
	}
	orderValue = "timeValue";
	alldata.array.sort(function(a,b) {return (a[orderValue] > b[orderValue]) ? 1 : ((b[orderValue] > a[orderValue]) ? -1 : 0);} );	
	updateList(alldata.array);
	
}


function codeLatLng(lat, lng) {
	var latlng = new google.maps.LatLng(lat, lng);
	/*if(latlng.length){}
	else{latlng = new google.maps.LatLng(59.436961, 24.753575);}*/
	console.log(latlng);
	geocoder.geocode({'latLng': latlng}, function(results, status) {
	  if (status == google.maps.GeocoderStatus.OK) {
		if (results[1]) {
			//formatted address
			$("#orig").val(results[0].formatted_address);
		} else {
			alert("No results found");
		}
	  } else {
		alert("Geocoder failed due to: " + status);
	  }
	});
	
}

function updateData(locations){
	var destination = [];
	for(loc in locations){
		destination.push(locations[loc]);
	}
	service = new google.maps.DistanceMatrixService();
	service.getDistanceMatrix(
		{
			origins: [$("#orig").val()],
			destinations: destination,
			travelMode: google.maps.TravelMode.DRIVING,
			avoidHighways: false,
			avoidTolls: false
		}, 
		callback
	);
}

function updateCoordinates(data){
	coordinates = data;
}

function fillInAddress() {
  var place = autocomplete.getPlace();
  console.log(componentForm);
  for (var component in componentForm) {
	document.getElementById(component).value = '';
	document.getElementById(component).disabled = false;
  }
	document.getElementById("location").value = place.geometry.location;
  for (var i = 0; i < place.address_components.length; i++) {
	var addressType = place.address_components[i].types[0];
	if (componentForm[addressType]) {
	  var val = place.address_components[i][componentForm[addressType]];
	  
	  document.getElementById(addressType).value = val;
	}
  }
}

var results = [];
var ratings = [];
var locations = [];
var coordinates = {};
var orderValue = $("#orderby").val();
var pager = 1;
var limiter = 20;

$(document).on("click", "#loadAll",  function(){
	$("#responselist li").show();
});
$(document).on("click", ".commentWindow",  function(){
	var cal_id = $(this).attr("data-id");
	$.ajax({
		type: "POST",
		url: "ajax.php", //Relative or absolute path to response.php file
		data: {"id": cal_id, "action": "getComments"},
		dataType: "json",
		success: function(response) {
			$(".comments").html(response);
		}
	});
	return false;
});

function nextday(day){
	var dateVar = day;
	var dsplit = dateVar.match(/(\d+)/g);
	var daynext = new Date();
	daynext.setFullYear(dsplit[0], dsplit[1]-1, dsplit[2]); 
	daynext.setTime(daynext.getTime() + 86400000);
	var next = $.datepicker.formatDate('yy-mm-dd', daynext);
	return next;
}

function updateList(array){

	var html = '<li class="list-group-item" style="font-weight: 700 !important;"><?=$pages->total?>: '+array.length+
		'<div class="col-xs-6 col-sm-4 ol-md-3" style="float:right;">'+
			 '<select class="form-control" id="orderby" name="orderby" style="border:none;">'+
				'<option value="timeValue"><?=$pages->sort?></option>'+
				'<option value="timeValue"><?=$pages->sort_time?></option>'+
				'<option value="price"><?=$pages->sort_price?></option>'+
				'<option value="distance"><?=$pages->sort_distance?></option>'+
			 '</select>'+
		'</div></li>';
	var count = 0;
	for(i in array){
		count ++;
		html += '<li data-item="'+count+'" class="list-group-item" style="display:'+(count > limiter ? "none" : "block")+'">'+
					'<div class="media">'+
					  '<div class="media-body media-middle">'+
						'<div class="col-xs-12 col-sm-12 col-md-5">'+
							'<div class="col-xs-12 col-sm-12 col-md-12" style="padding:0px;">'+
								'<h4 class="media-heading">'+array[i]["svc_name"]+'</h4>'+
							'</div>'+
							'<div class="col-xs-6 col-sm-6 col-md-6" style="padding:0px;">'+
								'<h4 class="media-heading"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> '+array[i]["time"]+'</h4>'+
							'</div>'+
							'<div class="col-xs-6 col-sm-6 col-md-6">'+
								'<p><i>'+array[i]["price"]+'� / '+array[i]["duration"]+'</i></p>'+
							'</div>'+
							'<div class="col-xs-6 col-sm-6 col-md-6" style="text-align: left;padding:0px;">'+
								'<p><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> <i>'+(array[i]["date"] == '<?=date("Y-m-d")?>' ? '<?=$pages->today?>: ' : '')+
								(array[i]["date"] == '<?=date("Y-m-d", strtotime("+1 day"))?>' ? '<?=$pages->tomorrow?>: ' : '')+array[i]["date"]+'</i> </p>'+
							'</div>'+
							
						'</div>'+
						'<div class="col-xs-6 col-sm-4 col-md-3">'+
							//'<p data-toggle="modal" data-target="#commentModal" class="commentWindow" data-id="'+array[i]["calendar_id"]+'"><i><span class="glyphicon glyphicon-star" aria-hidden="true"></span>'+array[i]["rating"]+' ('+array[i]["ratingCount"]+')</i></p>'+
							'<p><i><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span><a target="_blank" href="http://maps.google.com/?q='+array[i]["address"]+', '+array[i]["city"]+'">'+array[i]["address"]+', '+array[i]["city"]+'</a></i></p>'+
							'<p><i><span class="glyphicon glyphicon-road" aria-hidden="true"></span> '+array[i]["distance"]+' km</i></p>'+
							'<p><i><span class="glyphicon glyphicon-phone" aria-hidden="true"></span>'+array[i]["phone"]+'</i></p>'+
						'</div>'+
						'<div class="col-xs-6 col-sm-4 col-md-2">'+
							'<p><i>'+array[i]["company"]+'</i></p>'+
							'<p><i>'+array[i]["person"]+'</i></p>'+
						'</div>'+
						'<div class="col-xs-8 col-sm-4 col-md-2">'+
						'<a data-cal="'+array[i]["calendar_id"]+'" data-duration="'+array[i]["duration"]+'" data-time="'+array[i]["time"]+'"  data-date="'+array[i]["date"]+'"  data-service="'+array[i]["service"]+'" class="btn btn-default bookthis usercolor" ><?=$pages->bookingBtn?></a><br/>'+
						'<a target="_blank" href="https://www.booklux.com/'+array[i]["link"]+'"><?=$pages->more_options?></a>'+
					  '</div>'+
					  '</div>'+
					  
					'</div>'+
				'</li>';
	}
	html += '<li class="list-group-item">'+
					'<div class="media">'+
					  '<div class="media-body media-middle" style="text-align:center;">'+
						'<div class="col-xs-12 col-sm-12 col-md-12"><div id="loadAll" style="width:100%" class="btn btn-lg btn-info">Lae veel ...</div></div></div></div></li>';
						
						
	//alert(html);
	$("#responselist").html(html);
}




$(document).ready(function() {

	$("[name=location]").val("<?=$cityOption?>");
	initAutocomplete();
	$(window).scroll(function () {
		if ($(document).height() <= $(window).scrollTop() + $(window).height()) {
			start_item = pager*limiter;
			end_items = pager*limiter+limiter;
			while(start_item < end_items){
				$("[data-item="+start_item+"]").fadeIn(2000);
				start_item ++;
			}
			pager ++;
		}
	});
	
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
		return pattern.test(emailAddress);
	};
	
	$('#contact_form').submit(function() {
		
		var name = $("#name").val();
		var cemail = $("#form_mail").val();
		var message = $("#message").val();
		
		if(name == "" || cemail == ""  || message == ""){
			alert("K�ik lahtrid peavad olema korrektselt t�idetud!");
			return false;
		}
		
		//alert(message);
		$.ajax({
		  type: "POST",
		  url: "sendemail.php", //Relative or absolute path to response.php file
		  data: {"name": name, "email": cemail, "message": message},
		  success: function(response) {
			 //$("#content").html(response);
			 $(".success").fadeIn().delay(2000).fadeOut();
			//$("#content").css("width", "340px");
		  }
		});
		return false;
	});

	
	$( ".category_img" ).hover(
	  function() {
		$( this ).find(".listbox").css( "background-position", "80% 50%" );
	  }, function() {
		$( this ).find(".listbox").css( "background-position", "50% 50%" );
	  }
	);
	
	$("[name=group_select]").change(function(){
		window.location.href = "http://www.ilukalender.ee/?g="+$(this).val();
	});
	
	$(window).resize(function(){
		$(".listbox, .lb_overlay").css("height", $(".listbox").width());
		$(".listbox, .lb_overlay").css("line-height", $(".listbox").width()+"px");
		$(".listbox, .lb_overlay").css("border-radius", $(".listbox").width()/2);
		$(".lb_overlay").css("width", $(".listbox").width()+"px");	
	});
	
	$(".listbox, .lb_overlay").css("height", $(".listbox").width());
	$(".listbox, .lb_overlay").css("line-height", $(".listbox").width()+"px");
	$(".listbox, .lb_overlay").css("border-radius", $(".listbox").width()/2);
	$(".lb_overlay").css("width", $(".listbox").width()+"px");
	
	
	$('.terms').change(function() {
		if($(this).is(':checked')){
			$(".register").removeClass("disabled");
		} else{
			$(".register").addClass("disabled");
		}
	});
	
	$('[name=location]').change(function() {
		if($(this).val() == "Tallinn"){
			$("#borough").show();
		} else{
			$("#borough").hide();
		}
	});
	
	$('.terms_book').change(function() {
		if($(this).is(':checked')){
			$(".register_book").removeClass("disabled");
		} else{
			$(".register_book").addClass("disabled");
		}
	});
	
	$("[name=newtype]").change(function(){
		
		if($(this).val()==2){
			$("#user_form").hide();
			$("#company_form").show();
			window.open('https://www.booklux.com/ee/index.php?type=salon&lang=et');
		}else{
			$("#user_form").show();
			$("#company_form").hide();
		}
	});
	$("#forgotLink").click(function() {$("#forgotForm").show();});
	
	$('#dp3').datepicker({
		format: "yyyy-mm-dd",
		weekStart: 1,
		startDate: "<?=date("Y-m-d")?>",
		orientation: "auto",
		language: "et",
		autoclose: true
	});
	
	$(".ajax-form").submit(function() {
		var fullnumber = $("#mobile-number").val();
		$("#fullnumber").val(fullnumber);
		$(".overlay").show();
		//$("#responselist").html("<img src='images/ajax.gif' alt='ajax load' />");
		var formData = $(this).serialize();
		$.ajax({
			type: "POST",
			url: "ajax.php", //Relative or absolute path to response.php file
			data: formData,
			success: function(response) {
				if(response == 'forwarded'){
					$("#responselist").html('<li class="list-group-item list-group-item-success"><?=$pages->sentText?></li>');
				} else if(response == 'confirmed'){
					$("#responselist").html('<li class="list-group-item list-group-item-success"><?=$pages->verificationText?></li>');
				}
				else{$("#responselist").html('<li class="list-group-item list-group-item-warning"><?=$pages->failText?></li>');}
				$("#addBookingForm").modal('hide');
				$(".overlay").hide();
			}, 
			error: function(response){
				console.log(response);
				$("#responselist").html('<li class="list-group-item list-group-item-success"><?=$pages->response_error?>.</li>');
				$(".overlay").hide();
			}
			
		});
		//alert("<?=$pages->verificationText?>");
		//alert("Broneerida saab alates kuup�evast 07.05.2016");
		$("#addBookingForm").modal('hide');
		$(".overlay").hide();
		$("#responselist").html('<li class="list-group-item list-group-item-success"><?=$pages->verificationText?></li>');
		return false;
	});
	
	function sortData(){
		orderValue = $("#orderby").val();
		alldata.array.sort(function(a,b) {return (a[orderValue] > b[orderValue]) ? 1 : ((b[orderValue] > a[orderValue]) ? -1 : 0);} );
		updateList(alldata.array);
	}
	
	$(document).on("change", "#orderby", function() {
		sortData();
	});
	
	$("body").css("background-image", "url(images/bggif.gif)");
	
	$("#searchroom").submit(function() {
		if($("[name=service]").val() == ""){
			alert("<?=$pages->choose_service_first?>!");
			return false;
		}
		$(".overlay").show();
		var formData = $("#searchroom").serialize();
		
		$.ajax({
			type: "POST",
			url: "response9.php", //Relative or absolute path to response.php file
			data: formData,
			dataType: "json",
			success: function(response) {
				$('html, body').animate({
						scrollTop: $("#responselist").offset().top
				}, 1000);
				if(response.code != 0){
					alldata = response;
					updateData(response.locations);
				} 
				else{	
					$("#responselist").html('<li class="list-group-item list-group-item-danger"><?=$pages->noAvailableTimes?></li>');
				}
				$(".overlay").hide();
			}, 
			error: function(response){
				//console.log(response);
			}
		});
		return false;
	});
	
	var day = new Date(); today = $.datepicker.formatDate('yy-mm-dd', day);
	$('#date').datepicker('setDate', today);
	$('#datefrom').val(today);
	$('#dateto').val(nextday(today));
	$(".closebtn").click(function() {
		$("#responselist").show();
		$("#bookform").hide();
	});
	
	$('.svc_option').on("click", function(){
		//alert($(this).attr("data-id"));
		$("[name=service]").val($(this).attr("data-id"));
		$(".status").fadeIn().delay(1000).fadeOut();
	});
	
	
	

	$( "#slider-range" ).slider({
	  range: true,
	  min: 0,
	  max: 500,
	  values: [ 0, 500 ],
	  slide: function( event, ui ) {
		$( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$("[name=pricefrom]").val(ui.values[ 0 ]);
		$("[name=priceto]").val(ui.values[ 1 ]);
	  }
	});

	 $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
		  " - $" + $( "#slider-range" ).slider( "values", 1 ) );
	  
	$("#mobile-number").intlTelInput({defaultCountry: "ee", autoHideDialCode: false, nationalMode: false});
	
	$(document).on("click", '.bookthis', function(event) {
		$("#addBookingForm").modal('show');
		$("#setService").val($(this).attr("data-service"));
		$("#setDate").val($(this).attr("data-date"));
		$("#setTime").val($(this).attr("data-time"));
		$("#setCal").val($(this).attr("data-cal"));
		$("#setDuration").val($(this).attr("data-duration"));
	});
	
	
});

$(window).on("load", function() {
	$(".loader").hide();
});
</script>