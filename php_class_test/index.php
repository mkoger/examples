<!DOCTYPE html>
<html lang="et">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <title>Elektritarbimisajalugu</title>
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">
	<link id="bs-css" href="https://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
	<style type="text/css">
		.form-control[readonly]{
			cursor: default;
			background:#FFFFFF;
		}
	</style>
  </head>

  <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">FinestMedia</a>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row">        
        <div class="col-sm-12">
          <h1 class="page-header">Elektritarbimisajalugu</h1>
			<form id="getData">
				<div class="row">
				  <div class="col-sm-3"><input readonly="true" value="<?="01-".date("m-Y")?>" type="text" name="start" class="form-control datepicker"/></div>
				  <div class="col-sm-3"><input readonly="true" value="<?=date("d-m-Y", strtotime("-1 day"))?>" type="text" name="end" class="form-control datepicker"/></div>
				  <div class="col-sm-2">
					<select name="method" class="form-control">
						<option value="d F Y">Päev</option>
						<option value="W">Nädal</option>
						<option value="F Y">Kuu</option>
					</select>
				  </div>
				  <div class="col-sm-2"><input placeholder="kW/h" type="text" name="price" class="form-control"/></div>
				  <div class="col-md-2"><input type="submit" name="sub" class="btn btn-primary" value="otsi"/></div>
				</div>
			</form>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>  
				<tr><th>Aeg</th><th>Elektrikulu</th><th>Rahaline kulu</th></tr>			  
              </thead>
              <tbody id="result">
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
	<script src="https://uxsolutions.github.io/bootstrap-datepicker/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script>
		$(document).ready(function() {
			$('.datepicker').datepicker({
				startDate: "<?=date("d-m-Y", strtotime("-2 years"))?>",
				endDate: "<?=date("d-m-Y", strtotime("-1 day"))?>",
				format: "dd-mm-yyyy"
			});
			
			$("#getData").submit(function() {
				var formData = $(this).serialize();
				$.ajax({
					type: "POST",
					url: "ajax.php",
					data: formData,
					dataType: "json",
					success: function(response) {
						var content = "";
						if(response.error == ""){
							for(k in response.data){
								content += "<tr><td>"+k+"</td><td>"+response.data[k][0]+"</td><td>"+response.data[k][1]+" €</td></tr>";
							}
						} else{
							content = "<tr><td colspan='3'><h2>"+response.error+"</h2></td></tr>";
						}
						$("#result").html(content);
					}, 
					error: function(response){
						console.log(response);
					}
				});
				return false;
			});
		});
	</script>
  </body>
</html>
