<?php

// icon class for weather
function getIcon($value){
	$icons = array(
		"01d" => "wi-day-sunny", //clear sky
		"02d" => "wi-day-cloudy", //few clouds
		"03d" => "wi-day-cloudy", //scattered clouds
		"04d" => "wi-day-cloudy", //broken clouds
		"09d" => "wi-day-rain", //shower rain
		"10d" => "wi-day-rain", //rain
		"11d" => "wi-day-thunderstorm", //thunderstorm
		"13d" => "wi-day-snow", //snow
		"50d" => "wi-day-haze", //mist
		"01n" => "wi-night-clear", //clear sky
		"02n" => "wi-night-cloudy", //few clouds
		"03n" => "wi-night-cloudy", //scattered clouds
		"04n" => "wi-night-cloudy", //broken clouds
		"09n" => "wi-night-rain", //shower rain
		"10n" => "wi-night-rain", //rain
		"11n" => "wi-night-thunderstorm", //thunderstorm
		"13n" => "wi-night-snow", //snow
		"50n" => "wi-night-haze" //mist
	);
	return $icons[$value];
}
	

// cache file name	
function cacheFile(){
	
	return "caches/".md5(getRealIpAddr());

}


// current ip aadress for cache file
function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
	  $ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
	  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	   $ips = explode(',', $ip);
	   $ip =trim($ips[count($ips) - 1]);
	}
	else
	{
	  $ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
