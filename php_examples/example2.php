<?php
ob_start();
session_start();
//$yhendus=new mysqli("localhost", "root", "", "booklux");

function arrayString($sql, $key, $separation){
	$string = array();
	foreach(getArray($sql) as $object){
		array_push($string, $object[$key]);
	}
	$string = implode($separation, $string);
	return $string;
}

function getTranslation($lang, $abbr = "salons"){
	global $common_sql;
	$tr = new stdClass();
	$array = $common_sql->query("SELECT var, $lang FROM translation ORDER by var desc");
	if($array->num_rows > 0){
		while($arrayObject = $array->fetch_assoc()) {
			$variable = str_replace("@_".$abbr."_", "", $arrayObject["var"]);
			$tr->$variable = $arrayObject[$lang];
		}
	}
	return $tr;
}

function bookingConfirm($language, $reply, $recipient, $name, $service, $getdate, $gettime, $company, $employee, $bid = false){
	
	$settings = getArray("SELECT * FROM userSettings WHERE user='".$reply."'")[0];
	$name = ucwords(strtolower($name));
	$tr = getTranslation("et");
	$meil = "info@booklux.com"; //senders e-mail adress 
	$mail_body = $tr->letter_hello." ".$name."! \r\n\r\n";
	$mail_body .= $tr->letter_confirmed." \r\n";
	$mail_body .= $tr->letter_service.": ".$service." \r\n";
	$mail_body .= $tr->letter_employee.": ".$employee." \r\n";
	$mail_body .= $tr->letter_date.": ".$getdate." \r\n";
	$mail_body .= $tr->letter_time.": ".$gettime." \r\n\r\n";
	if($bid && $settings["cancellation"] == 1){
		$mail_body .= $tr->letter_cancellation.": https://www.booklux.com/cancel.php?uemail=".$recipient."&cemail=".$reply."&bid=".$bid." \r\n\r\n";
	}
	$mail_body .= $tr->letter_footer;
	$mail_body .= strlen($settings["email_footer"]) > 3 ? $settings["email_footer"] : $settings["company"];
	$subject = $tr->letter_confirmed; //subject 	
	sendEmail($company, $meil, $recipient, $mail_body, $subject, $reply);
	
}

function sendNewSms($number, $name, $text){
	require ( "sms/src/NexmoMessage.php" );
	$url = 'https://rest.nexmo.com/sms/json?' . http_build_query([
		'api_key' => '67d2f612',
		'api_secret' => 'dfcc8299',
		'to' => $number,
		'from' => $name,
		'text' => $text
	]);
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response = curl_exec($ch);
}

function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
	  $ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
	  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	   $ips = explode(',', $ip);
	   $ip =trim($ips[count($ips) - 1]);
	}
	else
	{
	  $ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
	
function getObject($sql){
	global $yhendus;
	$object = $yhendus->query($sql);	
	if($object->num_rows > 0){
		return $object->fetch_object();
	} 
	return false;
}

function getArray($sql){
	global $yhendus;
	$array = $yhendus->query($sql);
	$dataArray = array();
	if($array->num_rows > 0){
		while($arrayObject = $array->fetch_assoc()) {
			array_push($dataArray, $arrayObject);
		}
	}
	return $dataArray;
}

function timesList($interval = 15, $from = "00:00", $to = "00:00", $length = "00:00", $null = false){
	$array = array();
	if($null){array_push($array, "00:00");}
	$start = new DateTime("2015-01-01 ".$from.":00");
	$end = new DateTime("2015-01-01 ".$to.":00");
	
	if($to == "00:00"){
		$end = new DateTime("2015-01-01 ".$from.":00");
		$lengthparts = explode(":", $length);
		$end->add(new DateInterval('PT'.$lengthparts[0].'H'.$lengthparts[1].'M'));
	}
	
	while($start->format('H:i') < $end->format('H:i')){
		array_push($array, $start->format('H:i'));
		$start->add(new DateInterval('PT'.$interval.'M'));
	}
	
	return $array;
}

  function info($sentence){
    echo "<div class='alert alert-info alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>$sentence</div>";
  }
    function warn($sentence){
    echo "<div class='alert alert-danger alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>$sentence</div>";
  }
  function success($sentence){
    echo "<div class='alert alert-success alert-dismissable'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>$sentence</div>";
  }
  
  function curPageURL() {
	 $pageURL = 'http';
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 } else {
	  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 }
	 return $pageURL;
	} 